//by convention classed should begin in uppercase letter
class Student{
    // the constructor method defines our objects created from this class will be assigned their initial property values
    constructor(name, email, grades){
        this.name = name
        this.email = email
        this.gradesAve = undefined
        this.hasPassed = undefined
        this.passedWithHonors="no evaluation yet"
        
        if(grades.length === 4){
            if(grades.every(grade=> grade >= 0 && grade <=100)){
                this.grades = grades
            }else{
                this.grades = undefined
            }
        }else{
            this.grades = undefined
        }
    }

    login(){
        console.log(`${this.email} has logged in`)
        return this //to chain methods
    }//no commass
    logout(){
        console.log(`${this.email} has logged out.`)
        return this
    }
    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade)  
        })
        return this
    }
    computeAverage(){
        //this.gradesAve = sum/4;
        this.gradesAve = this.grades.reduce((a,b)=> a+b)/4;// this has constructor
        return this//for chainability
    }
    willPass(){
        this.hasPassed = this.gradesAve >=85 ? true : false;
        return this
    }
    willPassWithHonors(){
        if( this.gradesAve >=90){
            this.passedWithHonors = true;
        } else if( this.gradesAve >=85 && this.gradesAve < 90){
            this.passedWithHonors=false
        } else{
            this.passedWithHonors=undefined
        }
        return this
    }

}
//instantiate/create objects from our Student class
let studentOne = new Student("John","john@mail.com", [89,84,78,88])
let studentTwo = new Student("Joe","joe@mail.com", [78, 82, 79, 85])
let studentThree = new Student("Jane","jane@mail.com", [87, 89, 91, 93])
let studentFour = new Student("Jessie","jessie@mail.com", [91, 89, 92, 93])

// console.log(studentOne)
// console.log(studentTwo)
// console.log(studentThree)
// console.log(studentFour)