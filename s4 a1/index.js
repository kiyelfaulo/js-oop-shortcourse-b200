class Student {
	constructor(name, email, grades){
		this.name = name
		this.email = email
		this.gradeAve = undefined
		this.passed = undefined
		this.passedWithHonors = undefined

		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
				this.grades = grades
			}else{
				this.grades = undefined
			}
		}else{
			this.grades = undefined
		}
	}

	login(){
		console.log(`${this.email} has logged in.`)
		return this
	}

	logout(){
		console.log(`${this.email} has logged out.`)
		return this
	}

	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
		return this
	}

	computeAve(){
		let sum = 0
		this.grades.forEach(grade => sum = sum + grade)
		this.gradeAve = sum/4;
		return this
	}

	willPass(){
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this
	}

	willPassWithHonors(){
		if(this.computeAve().willPass().gradeAve >= 90){
			this.passedWithHonors = true
		}else if(this.computeAve().willPass().gradeAve < 90 && this.willPass().computeAve().gradeAve >= 85){
			this.passedWithHonors = false
		}else if(this.computeAve().willPass().gradeAve < 85){
			this.passedWithHonors = undefined
		}

		return this
	}
}

class Section {
	constructor(name){
		this.name = name
		this.students = []
		this.honorStudents = undefined
		this.honorsPercentage = undefined
	}

	addStudent(name, email, grades){
		this.students.push(new Student(name, email, grades))
		return this
	}

	countHonorStudents(){
		let count = 0

		this.students.forEach(student => {
			if(student.willPassWithHonors().passedWithHonors){
				count++
			}
		})

		this.honorStudents = count
		return this
	}

	computeHonorsPercentage(){
		let percentage = (this.countHonorStudents().honorStudents / this.students.length) * 100
		this.honorsPercentage = `${percentage}%`
		return this
	}
}

class Grade {
	constructor(level){
		this.level = level
		this.sections = []
		this.totalStudents = 0
		this.totalHonorStudents = 0
		this.batchAveGrade = undefined
		this.batchMinGrade = undefined
		this.batchMaxGrade = undefined
	}

	addSection(name){
		this.sections.push(new Section(name))
		return this
	}

    countStudents(){
        this.sections.forEach(section => {
            this.totalStudents += section.students.length         
        });
        return this   
    }

    countHonorStudents(){
        this.sections.forEach(section => {
            this.totalHonorStudents += section.countHonorStudents().honorStudents
        });
        return this
    }
    computeBatchAve(){
        this.batchAveGrade = 0
        this.sections.forEach(section => {
            section.students.forEach(student =>{
               this.batchAveGrade += student.computeAve().gradeAve
            })
        });  

        this.batchAveGrade = this.batchAveGrade / this.countStudents().totalStudents;
        return this
        //console.log(this.sections)
    }

    getBatchMinGrade(){
        let allGrades = []
        this.sections.forEach(section => {
            section.students.forEach(student =>{
                allGrades = [...allGrades,...student.grades]
            })
        });
        this.batchMinGrade = Math.min(...allGrades)
        return this
    }
    getBatchMaxGrade(){
        let allGrades = []
        this.sections.forEach(section => {
            section.students.forEach(student =>{
                allGrades = [...allGrades,...student.grades]
            })
        });
        this.batchMaxGrade = Math.max(...allGrades)
        return this
    }
}

let grade1 = new Grade(1)

grade1.addSection('section1A')
grade1.addSection('section1B')
grade1.addSection('section1C')
grade1.addSection('section1D')

let section1A = grade1.sections.find(section => section.name === "section1A")
let section1B = grade1.sections.find(section => section.name === "section1B")
let section1C = grade1.sections.find(section => section.name === "section1C")
let section1D = grade1.sections.find(section => section.name === "section1D")

section1A.addStudent("John", "john@mail.com",[89, 84, 78, 88])
section1A.addStudent("Joe", "joe@mail.com", [78, 82, 79, 85])
section1A.addStudent("Jane", "jane@mail.com", [87, 89, 91, 93])
section1A.addStudent("Jessie", "jessie@mail.com", [91, 89, 92, 93])

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);

console.log(grade1)