class Customer {
	constructor(email){
		this.email = email
		this.cart = new Cart()
		this.orders = []
		
	}

	checkOut(){

			this.orders.push({products: this.cart.contents, totalAmount: this.cart.computeTotal().totalAmount})
		return this
	}

}

class Cart{
	constructor(){
		this.contents = []
		this.totalAmount = 0
		
	}

	addToCart(product, qty){
		this.contents.push({product: product, quantity: qty})
	}
	showCartContents(){
		return this.contents
	}
	updateProductQuantity(itemName, newQty){

		const itemIndex = this.contents.findIndex(item => item.product.name ===itemName)
		this.contents[itemIndex].quantity = newQty 
	}
	clearCartContents(){
		this.contents = []
		this.totalAmount = 0
		return this
	}
	computeTotal(){

		this.contents.forEach(item =>{
			this.totalAmount += item.product.price*item.quantity
		})

		return this
		
	}

}

class Product{
	constructor(name, price){
		this.name = name
		this.price = price
		this.isActive = true
	}

	archive(){
		this.isActive = false
		return this
	}

	updatePrice(newPrice){
		this.price = newPrice
		return this
	}
	

}

const john = new Customer("john@gmail.com");
const prodA = new Product("soap", 9.99);
const prodB = new Product("shampoo", 5.50);


john.cart.addToCart(prodA, 5)
john.cart.addToCart(prodB, 2)
//john.cart.updateProductQuantity('soap', 500)



